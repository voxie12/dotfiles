local wezterm = require 'wezterm'

local config = {}
-- use config builder object if possible
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- settings
config.color_scheme = 'tokyonight'
config.font_size = 16
config.font = wezterm.font_with_fallback({
  { family = 'Iosevka Nerd Font', scale = 1 },
  { family = 'Hack Nerd Font', scale = 1 },
}) 
config.window_background_opacity = 0.9
config.window_decorations = "RESIZE"
config.window_close_confirmation = "AlwaysPrompt"
config.scrollback_lines = 3000

config.hide_tab_bar_if_only_one_tab = true
config.hide_mouse_cursor_when_typing = true
config.default_cursor_style = 'SteadyUnderline'


-- and finally, return the configuration to wezterm
return config

