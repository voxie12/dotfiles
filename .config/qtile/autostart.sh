#!/usr/bin/env bash 

xset r rate 300 50 &
xset s off -dpms &
lxsession &
picom -f &
dunst &
udiskie &
unclutter &
~/.screenlayout/2mon.sh &
#xwallpaper --zoom ~/pic/wallhaven/wallhaven-dgrjdg.png &
xwallpaper --output DisplayPort-0 --zoom ~/pic/wallhaven/wallhaven-3l6999.jpg  \
           --output DisplayPort-2 --zoom ~/pic/wallhaven/wallhaven-d6o88l.jpg &
killall redshift
redshift &
telegram-desktop &
firefox &
