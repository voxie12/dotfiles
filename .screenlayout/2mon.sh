#!/bin/sh

xrandr --output DisplayPort-0 \
       --refresh 144 \
       --primary \
       --mode 1920x1080 \
       --pos 1920x0 \
       --rotate normal \
       --output DisplayPort-1 \
       --off \
       --output DisplayPort-2 \
       --refresh 144 \
       --mode 1920x1080 \
       --pos 0x0 \
       --rotate normal \
       --output HDMI-A-0 \
       --off
