export EDITOR="nvim"
export VISUAL="nvim"
export TERM="xterm-256color"
export HISTORY_IGNORE="(ls|cd|pwd|exit|reboot|off|history|cd -|cd ..)"
export TERMINAL="alacritty"
#export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANWIDTH=90
export BROWSER="firefox"

export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:$HOME/.cargo/bin
export PATH=$PATH:$HOME/go/bin

