#!/bin/bash

xset r rate 250 50 &
xset s off -dpms &
xwallpaper --zoom ~/pic/wallhaven/wallhaven-dgrjdg.png &
lxsession &
dunst &
udiskie &
redshift &
#killall slstatus
slstatus &
#. ~/.local/bin/dwm2 &
sxhkd &
unclutter &
#xcompmgr &
picom -f &
#/home/jay/.screenlayout/144fps.sh &
. ~/.screenlayout/2mon.sh &
#/usr/bin/pipewire & 
#/usr/bin/pipewire-pulse & 
#/usr/bin/pipewire-media-session &

