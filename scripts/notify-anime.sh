#!/bin/sh

#currenttime=$(date +%H:%M%a)
#
## list of anime on monday
#if [[ "$currenttime" < "14:30" ]]; then 
#  notify-send "Classroom of the Elite S2" "is out now"
#else
#  echo
#fi
#
#if [[ "$currenttime" < "17:41" ]]; then 
#  notify-send "test" "is out now"
#fi
#
## list of anime on tuesday 
#if [[ "$currenttime" < "15:00Tue" ]]; then 
#  notify-send "Kinsou no Vermeil" "is out now"
#fi 
while true
do
  sleep 60
case $(date +%H:%M%a) in

    # monday
    (14:30Mon)  notify-send "Classroom of the Elite S2" "is out now";;
    # tuesday
    (15:00Tue)  notify-send "Kinsou no Vermeil" "is out now";;
    (19:00Tue)  notify-send "Jashin-chan Dropkick" "is out now";;
    # wednesday
    (15:30Wed)  notify-send "Made in Abyss" "is out now";;
    (16:00Wed)  notify-send "My Stepmom's Daughter Is My Ex" "is out now";;
    (16:30Wed)  notify-send "Slave Harem in the Labyrinth of the Other World" "is out now";;
    # thursday
    (15:00Thu)  notify-send "Danmachi" "is out now";;
    (15:30Thu)  notify-send "The Devil Is a Part-Timer!" "is out now";;
    (18:30Thu)  notify-send "Call of the Night" "is out now";;
    # friday
    (17:00Fri)  notify-send "When Will Ayumu Make His Move?" "is out now";;
    (17:30Fri)  notify-send "Shadow House S2" "is out now";;
    (19:30Fri)  notify-send "Rent-A-Girlfriend" "is out now";;
    # saturday
    (17:00Sat)  notify-send "Lycoris Recoil" "is out now";;
    (18:00Sat)  notify-send "Engage Kiss" "is out now";;
    (19:00Sat)  notify-send "A Couple of Cuckoos" "is out now";;
    (19:45Sat)  notify-send "The Maid I Hired Recently is Mysterious" "is out now";;
    # sunday
    (03:00Sun)  notify-send "One Piece" "is out now";;

    # test
    #(18:09Fri)  notify-send "test" "is out now";;
    #(0[012345]:*) echo YES;;
    #(06:[012]*)   echo YES;;
    #(*)           echo NO;;
esac
done
