#!/bin/bash

# init
printf "$$" > ~/.cache/subsplease.txt
sec=0

FILE1=~/.cache/subsplease/index.html
FILE2=~/.cache/subsplease/index2.html
FILE3=~/.cache/subsplease/index3.html

refresh () {
  curl -s https://subsplease.org/rss/ > $FILE1
  xmllint --xpath "//title" $FILE1 > $FILE2
  sed -e 's/........................title>//g' $FILE2 > $FILE3
  cat $FILE2 | sed 's/<title>.............//g' | sed 's/.............................title>//g' > $FILE3
}

c() { printf "\033[%s;%sm" "$1" "$2"; }

clock () {
  date
  printf "%s\n"
}

# list of anime
one-piece () {
  if grep -q "One Piece" $FILE3 ; then
    printf "%s\n" "2:00 - $(c 1 31)Available $(c 0 0)- One Piece"
  else
    printf "%s\n" "2:00 - $(c 1 30)Unavailable $(c 0 0)- One Piece"
  fi
}

riverdale () {
    printf "%s\n" "8:00 $(c 0 0)- Riverdale"
}

mk() {
    printf "%s\n" "8:00 $(c 0 0)- Moon Knight"
}
awh () {
  if grep -q "Aharen-san wa Hakarenai" $FILE3 ; then
    printf "%s\n" "19:15 - $(c 1 32)Available $(c 0 0)- Aharen-san wa Hakarenai"
  else
    printf "%s\n" "19:15 - $(c 1 31)Unavailable $(c 0 0)- Aharen-san wa Hakarenai"
  fi
}

#one-piece () {
#  if [ "One Piece" = '~/.cache/subsplease/index3.html' ]; then
#    printf 'available
#  else
#    
#}

m () {
  printf "%s\n" "$(c 0 0)---------------Monday -----------------$(c 0 0)"
}
t () {
  printf "%s\n"
  printf "%s\n" "$(c 0 0)---------------Tuesday ---------------$(c 0 0)"
}
w () {
  printf "%s\n"
  printf "%s\n" "$(c 0 0)---------------Wednesday -----------------$(c 0 0)"
}
th () {
  printf "%s\n"
  printf "%s\n" "$(c 0 0)---------------Thursday -----------------$(c 0 0)"
}
f () {
  printf "%s\n"
  printf "%s\n" "$(c 0 0)---------------Friday ----------------$(c 0 0)"
}
sa () {
  printf "%s\n"
  printf "%s\n" "$(c 0 0)---------------Saturday ----------------$(c 0 0)"
}
su () {
  printf "%s\n"
  printf "%s\n" "$(c 0 0)---------------Sunday ------------------$(c 0 0)"
}

display () {
  clear
  clock
  m 
  t 
  riverdale
  w 
  mk
  th 
  f 
  awh
  sa 
  su 
  one-piece
}

while true
do 
  sleep 1 & wait && {
    [ $((sec % 60 )) -eq 0 ] && refresh
    [ $((sec % 5 )) -eq 0 ] && display
    sec=$((sec + 1))
  }
done
