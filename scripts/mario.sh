#!/bin/bash

#  $(c 1 31)████ red
#  $(c 1 32)████ green
#  $(c 1 33)████ yellow
#  $(c 1 34)████ blue
#  $(c 1 35)████ purple
#  $(c 1 36)████ light blue
#  $(c 1 37)████ white
#  $(c 1 30)████ gray
#  $(c 0 30)████ black 

c() { printf "\033[$1;$2m"; }


#  $(c 1 31)████ red
#  $(c 1 32)████ green
#  $(c 1 33)████ yellow
#  $(c 1 34)████ blue
#  $(c 1 35)████ purple
#  $(c 1 36)████ light blue
#  $(c 1 37)████ white
#  $(c 1 30)████ gray
#  $(c 0 30)████ black 
mario () {
  printf "
  $(c 1 31)   █████ 
  $(c 1 31)  █████████
  $(c 1 32)  ███$(c 1 33)██$(c 1 32)█$(c 1 33)█
  $(c 1 32) █$(c 1 33)█$(c 1 32)█$(c 1 33)███$(c 1 32)█$(c 1 33)███
  $(c 1 32) █$(c 1 33)█$(c 1 33)██$(c 1 33)███$(c 1 33)█$(c 1 33)███
  $(c 1 33) ██████████
  $(c 1 33)   ███████
  $(c 1 33)  ██████
  $(c 1 33) ██████████
  $(c 1 33)████████████            
  $(c 1 33)████████████            
  $(c 1 33)████████████            
  $(c 1 33)████████████            
  $(c 1 31)  ███  ███
  $(c 1 32) ███    ███ 
  $(c 1 32)████    ████
  "
}


mario
